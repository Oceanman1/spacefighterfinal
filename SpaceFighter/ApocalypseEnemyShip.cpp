#include "ApocalypseEnemyShip.h"

ApocalypseEnemyShip::ApocalypseEnemyShip()
{
	SetSpeed(50);
	SetMaxHitPoints(20);
	SetCollisionRadius(20);
}


void ApocalypseEnemyShip::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();
	}
	
	EnemyShip::Update(pGameTime);
}


void ApocalypseEnemyShip::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
		
	}
}
