

#include "Level03.h"
#include "BioEnemyShip.h"
#include "InvaderEnemyShip.h"
#include "ApocalypseEnemyShip.h"


void Level03::LoadContent(ResourceManager* pResourceManager)
{
	// Setup enemy ships

	// Initialize Bio Ship Enemy
	Texture* pBioEnemyTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	const int BIOENEMYCOUNT = 62;

	double xBioEnemyPositions[BIOENEMYCOUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55,
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.5, 0.4, 0.6, 0.35, 0.65, 0.25, 0.75,
		0.5, 0.4, 0.6, 0.35, 0.65, 0.25, 0.75
	};

	double bioEnemyDelays[BIOENEMYCOUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3,
		4, 0.25, 0.25,
		4.25, 0.25, 0.25,
		4.5, 0.3, 0.3, 0.3, 0.3,
		5, 0.3, 0.3, 0.3, 0.3,
		5.25, 0.25, 0.25,
		5.5, 0.25, 0.25,
		5.75, 0.3, 0.3, 0.3, 0.3,
		8.5, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
		8.75, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3
	};

	float bioEnemyDelay = 1.0; // start delay

	Vector2 bioEnemyPosition;

	for (int i = 0; i < BIOENEMYCOUNT; i++)
	{
		bioEnemyDelay += bioEnemyDelays[i];
		bioEnemyPosition.Set(xBioEnemyPositions[i] * Game::GetScreenWidth(), -pBioEnemyTexture->GetCenter().Y);

		BioEnemyShip* pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pBioEnemyTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(bioEnemyPosition, (float)bioEnemyDelay);
		AddGameObject(pEnemy);
	}
	
	// Initiliaze Invader Enemy Ship
	Texture* pInvaderTexture = pResourceManager->Load<Texture>("Textures\\InvaderEnemyShip.png");

	const int INVADERCOUNT = 9;

	double xInvaderPositions[INVADERCOUNT] =
	{
		0.1, 0.5, 0.8,
		0.15, 0.3, 0.65,
		0.3, 0.25, 0.4
	};

	double invaderDelays[INVADERCOUNT] =
	{
		0.0, 2.0, 4.0,
		6.0, 8.0, 10.0,
		11.0, 12.0, 13.0
	};

	float invaderDelay = 5.0; // start delay

	Vector2 invaderPosition;

	for (int i = 0; i < INVADERCOUNT; i++)
	{
		invaderDelay += invaderDelays[i];
		invaderPosition.Set(xInvaderPositions[i] * Game::GetScreenWidth(), -pBioEnemyTexture->GetCenter().Y);

		InvaderEnemyShip* pEnemy = new InvaderEnemyShip();
		pEnemy->SetTexture(pInvaderTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(invaderPosition, (float)invaderDelay);
		AddGameObject(pEnemy);
	}

	// Initialize Apocalypse Enemy Ship

	Texture* pApocalypseTexture = pResourceManager->Load<Texture>("Textures\\ApocalypseEnemyShip.png");

	const int APOCALYPSECOUNT = 5;

	double xApocalypsePositions [APOCALYPSECOUNT] =
	{
		0.3, 0.65, 0.47,
		0.82, 0.43
	};

	double apocalypseDelays[APOCALYPSECOUNT] =
	{
		0.0, 6.0, 12.0,
		18.0, 27.0
	};

	float apocalypseDelay = 15; // start delay

	Vector2 apocalypsePosition;

	for (int i = 0; i < APOCALYPSECOUNT; i++)
	{
		apocalypseDelay += apocalypseDelays[i];
		apocalypsePosition.Set(xApocalypsePositions[i] * Game::GetScreenWidth(), -pBioEnemyTexture->GetCenter().Y);

		ApocalypseEnemyShip* pEnemy = new ApocalypseEnemyShip();
		pEnemy->SetTexture(pApocalypseTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(apocalypsePosition, (float)apocalypseDelay);
		AddGameObject(pEnemy);
	}
	
	Level::LoadContent(pResourceManager);
}
