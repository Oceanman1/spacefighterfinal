

#include "Level01.h"
#include "BioEnemyShip.h"


void Level01::LoadContent(ResourceManager* pResourceManager)
{
	// Setup enemy ships

	// Initialize Bio Ship Enemy
	Texture* pBioEnemyTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	const int BIOENEMYCOUNT = 21;

	double xBioEnemyPositions[BIOENEMYCOUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};

	double bioEnemyDelays[BIOENEMYCOUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	float bioEnemyDelay = 1.0; // start delay

	Vector2 bioEnemyPosition;

	for (int i = 0; i < BIOENEMYCOUNT; i++)
	{
		bioEnemyDelay += bioEnemyDelays[i];
		bioEnemyPosition.Set(xBioEnemyPositions[i] * Game::GetScreenWidth(), -pBioEnemyTexture->GetCenter().Y);

		BioEnemyShip* pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pBioEnemyTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(bioEnemyPosition, (float)bioEnemyDelay);
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}

