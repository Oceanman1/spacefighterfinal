#pragma once

#include "EnemyShip.h"

class ApocalypseEnemyShip : public EnemyShip
{

public:

	ApocalypseEnemyShip();
	virtual ~ApocalypseEnemyShip() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


private:

	Texture* m_pTexture;

};