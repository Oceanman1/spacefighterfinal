#pragma once
#include "EnemyShip.h"
// Added by Jason
class InvaderEnemyShip: public EnemyShip
{
public:

	InvaderEnemyShip();
	virtual ~InvaderEnemyShip() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


private:

	Texture* m_pTexture;
};

