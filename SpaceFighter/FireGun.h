
#pragma once

#include "SpecialWeapon.h"

class FireGun : public SpecialWeapon
{

public:

	FireGun(const bool isActive) : SpecialWeapon(isActive)
	{
		m_cooldown = 0;
		m_cooldownSeconds = 0.10;
	}

	virtual ~FireGun() { }

	virtual void Update(const GameTime *pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType specialTriggerType)
	{
		if (IsActive() && CanFire())
		{
			if (specialTriggerType.Contains(GetSpecialTriggerType()))
			{
				SpecialProjectile *pSpecialProjectile = GetSpecialProjectile();
				if (pSpecialProjectile)
				{
					pSpecialProjectile->Activate(GetPosition(), true);
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;

};
