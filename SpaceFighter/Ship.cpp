
#include "Ship.h"


Ship::Ship()
{
	SetPosition(0, 0);
	SetCollisionRadius(10);

	m_speed = 300;
	m_maxHitPoints = 3;
	m_isInvulnurable = false;

	Initialize();
}

void Ship::Update(const GameTime *pGameTime)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Update(pGameTime);
	}

	GameObject::Update(pGameTime);
}

void Ship::Hit(const float damage)
{
	// Check for target vulnurability to attack
	if (!m_isInvulnurable)
	{
		// If target is vulnurable, subtract damage amount from target hit points
		m_hitPoints -= damage;

		if (m_hitPoints <= 0)
		{
			// If hit points are less than or equal to zero, deactive target (Target killed!)
			GameObject::Deactivate();
		}
	}
}

void Ship::Initialize()
{
	m_hitPoints = m_maxHitPoints;
}

void Ship::FireWeapons(TriggerType type)
{
	// Set current weapon equal to first weapon in vector array. 
	m_weaponIt = m_weapons.begin();
	// Loop through weapon vectory array.
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		// Fire weapon for corresponding trigger key.
		(*m_weaponIt)->Fire(type);
	}
}

void Ship::FireSpecialWeapons(TriggerType type)
{
	// Set current specialweapon equal to first weapon in vector array. 
	m_specialWeaponIt = m_specialWeapons.begin();
	// Loop through weapon vectory array.
	for (; m_specialWeaponIt != m_specialWeapons.end(); m_specialWeaponIt++)
	{
		// Fire weapon for corresponding trigger key.
		(*m_specialWeaponIt)->Fire(type);
	}
}

void Ship::AttachWeapon(Weapon *pWeapon, Vector2 position)
{
	pWeapon->SetGameObject(this);
	pWeapon->SetOffset(position);
	m_weapons.push_back(pWeapon);
}

void Ship::AttachSpecialWeapon(SpecialWeapon *pSpecialWeapon, Vector2 position)
{
	pSpecialWeapon->SetGameObject(this);
	pSpecialWeapon->SetOffset(position);
	m_specialWeapons.push_back(pSpecialWeapon);
}