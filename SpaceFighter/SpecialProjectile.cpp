
#include "SpecialProjectile.h"

Texture *SpecialProjectile::s_pSpecialTexture = nullptr;

SpecialProjectile::SpecialProjectile()
{
	SetSpeed(500);
	SetDamage(1);
	SetDirection(-Vector2::UNIT_Y);
	SetCollisionRadius(9);

	m_drawnByLevel = true;
}

void SpecialProjectile::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		// Get velocity for direction projectile is fired
		Vector2 translation = m_direction * m_speed * pGameTime->GetTimeElapsed();
		// Move projectice in that direction
		TranslatePosition(translation);

		// Current position of projectile
		Vector2 position = GetPosition();
		// Current size of projectile
		Vector2 size = s_pSpecialTexture->GetSize();

		// Is the projectile off the screen?
		if (position.Y < -size.Y) Deactivate();  // off bottom of screen
		else if (position.X < -size.X) Deactivate();  // off left of screen
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate();  // off top of screen
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate();  // off right of screen
	}

	GameObject::Update(pGameTime);
}

void SpecialProjectile::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(s_pSpecialTexture, GetPosition(), Color::White, s_pSpecialTexture->GetCenter());
	}
}

void SpecialProjectile::Activate(const Vector2 &position, bool wasShotByPlayer)
{
	m_wasShotByPlayer = wasShotByPlayer;
	SetPosition(position);

	GameObject::Activate();
}

std::string SpecialProjectile::ToString() const
{
	return ((WasShotByPlayer()) ? "Player " : "Enemy ") + GetProjectileTypeString();
}

CollisionType SpecialProjectile::GetCollisionType() const
{
	CollisionType shipType = WasShotByPlayer() ? CollisionType::PLAYER : CollisionType::ENEMY;
	return (shipType | GetProjectileType());
}