
#include <string>
#include "MainMenuScreen.h"
#include "GameplayScreen.h"

// Callback Functions
void OnLevel01Select(MenuScreen *pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen());
}

void OnLevel02Select(MenuScreen* pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen());
}

void OnLevel03Select(MenuScreen* pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen());
}

void OnQuitSelect(MenuScreen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	pMainMenuScreen->SetQuitFlag();
	pMainMenuScreen->Exit();
}

void OnScreenRemove(Screen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	if (pMainMenuScreen->IsQuittingGame()) pScreen->GetGame()->Quit();
}



MainMenuScreen::MainMenuScreen()
{
	m_pTexture = nullptr;

	SetRemoveCallback(OnScreenRemove);

	SetTransitionInTime(2.0f);  // DTB changed from 1 second to 2 seconds
	SetTransitionOutTime(0.5f);

	Show(); // Show the screen
}

void MainMenuScreen::LoadContent(ResourceManager *pResourceManager)
{
	// Logo
	m_pTexture = pResourceManager->Load<Texture>("Textures\\Logo.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150;

	// Create the menu items
	const int COUNT = 4;
	MenuItem *pItem;
	Font::SetLoadSize(20, true);
	Font *pFont = pResourceManager->Load<Font>("Fonts\\SpyAgency.ttf");  // DTB changed font from Ethnocentric to Arial

	SetDisplayCount(COUNT);

	enum Items { LEVEL01, LEVEL02, LEVEL03, QUIT };
	std::string text[COUNT] = { "Level01", "Level02", "Level03", "Quit" };

	for (int i = 0; i < COUNT; i++)
	{
		pItem = new MenuItem(text[i]);
		pItem->SetPosition(Vector2(100, 100 + 50 * i));
		pItem->SetFont(pFont);
		pItem->SetColor(Color::Blue);
		pItem->SetSelected(i == 0);
		AddMenuItem(pItem);
	}

	GetMenuItem(LEVEL01)->SetSelectCallback(OnLevel01Select);
	GetMenuItem(LEVEL02)->SetSelectCallback(OnLevel02Select);
	GetMenuItem(LEVEL03)->SetSelectCallback(OnLevel03Select);
	GetMenuItem(QUIT)->SetSelectCallback(OnQuitSelect);
}

void MainMenuScreen::Update(const GameTime *pGameTime)
{
	MenuItem *pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		if (pItem->IsSelected()) pItem->SetColor(Color::Green);  
		else pItem->SetColor(Color::White); 
	}

	MenuScreen::Update(pGameTime);
}

void MainMenuScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}
