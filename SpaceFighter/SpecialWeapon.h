
#pragma once

#include "KatanaEngine.h"
#include "SpecialProjectile.h"
#include "TriggerType.h"

using namespace KatanaEngine;

class SpecialWeapon
{

public:

	SpecialWeapon(bool isActive = true)
	{
		m_isActive = isActive;
		SetSpecialTriggerType(TriggerType::SPECIAL);
	}

	virtual ~SpecialWeapon() { }

	virtual void Update(const GameTime *pGameTime) { };

	virtual void Draw(SpriteBatch *pSpriteBatch) { };

	virtual void Fire(TriggerType triggerType) = 0;  // EX: Pure Virtual

	virtual void SetGameObject(GameObject *pGameObject) { m_pGameObject = pGameObject; }

	virtual void SetOffset(Vector2 offset) { m_offset = offset; }

	virtual void SetSpecialTriggerType(TriggerType specialTriggerType) { m_specialTriggerType = specialTriggerType; }

	virtual void SetProjectilePool(std::vector<SpecialProjectile *> *pSpecialProjectiles) { m_pSpecialProjectiles = pSpecialProjectiles; }

	virtual void Activate() { m_isActive = true; }

	virtual void Dectivate() { m_isActive = false; }

	virtual bool IsActive() const { return m_isActive && m_pGameObject->IsActive(); }


protected:

	virtual TriggerType GetSpecialTriggerType() const { return m_specialTriggerType; }

	virtual Vector2 GetPosition() const { return m_pGameObject->GetPosition() + m_offset; }

	virtual SpecialProjectile *GetSpecialProjectile()
	{
		m_specialProjectileIt = m_pSpecialProjectiles->begin();
		for (; m_specialProjectileIt != m_pSpecialProjectiles->end(); m_specialProjectileIt++)
		{
			SpecialProjectile *pSpecialProjectile = *m_specialProjectileIt;
			if (!pSpecialProjectile->IsActive()) return pSpecialProjectile;
		}

		return nullptr;
	}


private:

	bool m_isActive;

	GameObject *m_pGameObject;

	Vector2 m_offset;

	TriggerType m_specialTriggerType;

	std::vector<SpecialProjectile *>::iterator m_specialProjectileIt;
	std::vector<SpecialProjectile *> *m_pSpecialProjectiles;

};
